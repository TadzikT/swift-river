﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

namespace Swift_River_for_Android
{

    public class SwiftRiver : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        RenderTarget2D renderTarget;
        RenderTarget2D worldRenderTarget;
        Assets assets;
        World world;
        Player player;
        List<Missile> missiles;
        List<Enemy> enemies;
        List<Explosion> explosions;
        List<Fuel> fuels;
        List<Alien> aliens;
        Gauge gauge;
        bool gameOver;
        public int score;
        int virtualWidth, virtualHeight;
        //touch input: iksy sa od 17 do 1062, igreki od 0 do 1827
        int touchXMin = 17, touchXMax = 1062, touchYMin = 0, touchYMax = 1827;
        int touchYUpThreshold, touchYDownThreshold;

        public SwiftRiver()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            Resolution.Init(ref graphics);
            Resolution.SetVirtualResolution(450, 800);
            Resolution.SetResolution(GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height, false);
            virtualWidth = 450;
            virtualHeight = 800;
            graphics.SupportedOrientations = DisplayOrientation.Portrait;
            graphics.ApplyChanges();
            touchYUpThreshold = (touchYMax - touchYMin) / 4;
            touchYDownThreshold = touchYMax - ((touchYMax - touchYMin) / 4);
            base.Initialize();
        }

        protected override void LoadContent()
        {

            renderTarget = new RenderTarget2D(graphics.GraphicsDevice, virtualWidth, virtualHeight, false, graphics.PreferredBackBufferFormat, DepthFormat.Depth24);
            worldRenderTarget = new RenderTarget2D(graphics.GraphicsDevice, virtualWidth, virtualHeight, false, graphics.PreferredBackBufferFormat, DepthFormat.Depth24);
            spriteBatch = new SpriteBatch(GraphicsDevice);
            assets = new Assets(Content);
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(assets.backgroundMusic);
            initGame();
        }

        void initGame()
        {
            missiles = new List<Missile>();
            enemies = new List<Enemy>();
            explosions = new List<Explosion>();
            fuels = new List<Fuel>();
            aliens = new List<Alien>();
            world = new World(virtualWidth, virtualHeight, assets);
            player = new Player(virtualWidth, virtualHeight, assets);
            gauge = new Gauge(virtualWidth, virtualHeight, assets);
            score = 0;
            gameOver = false;
            SoundEffect.MasterVolume = 0.3f;
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            TouchCollection touches = TouchPanel.GetState();
            if (gameOver)
            {
                bool touched = false;
                foreach (TouchLocation touch in touches)
                {
                    if (touch.Position.X < 680 && touch.Position.X > 400 && touch.Position.Y < 1040 && touch.Position.Y > 880)
                    {
                        touched = true;
                        initGame();
                    }
                }
                if (!touched)
                {
                    return;
                }
            }

            if (touches.Count == 0)
            {
                player.direction = 1;
                world.scrollSpeed = 5;
            }
            foreach (TouchLocation touch in touches)
            {
                if (touch.Position.Y < touchYUpThreshold || touch.Position.Y > touchYDownThreshold)
                {
                    player.direction = 1;
                    continue;
                }
                if (touch.Position.X < (touchXMax - touchXMin) / 2)
                {
                    player.direction = 0;
                }
                else if (touch.Position.X > (touchXMax - touchXMin) / 2)
                {
                    player.direction = 2;
                }
            }
            foreach (TouchLocation touch in touches)
            {

                if (touch.Position.Y > touchYUpThreshold && touch.Position.Y < touchYDownThreshold)
                {
                    continue;
                }
                if (touch.Position.Y < touchYUpThreshold)
                {
                    world.scrollSpeed = 8;
                }
                else if (touch.Position.Y > touchYDownThreshold)
                {
                    world.scrollSpeed = 2;
                }
            }

            if (Missile.readyToLaunch(gameTime.TotalGameTime))
            {
                assets.sound(assets.missileFired).Play();
                missiles.Add(new Missile(player.ship.X + 20, player.ship.Y - 5, assets));
                missiles.Add(new Missile(player.ship.X + 40, player.ship.Y - 5, assets));
            }

            world.update(enemies, fuels, aliens);
            missiles.RemoveAll(i => !i.update(world.scrollSpeed));
            enemies.RemoveAll(i => !i.update(world.scrollSpeed, virtualWidth, world.textureFlipsCounter, world.textureHeight));
            aliens.RemoveAll(i => !i.update(world.scrollSpeed));
            if (!player.update(worldRenderTarget) || !gauge.update(world.scrollSpeed))
            {
                gameOver = true;
            }
            else
            {
                gameOver = false;
            }
            missiles.RemoveAll(i => i.checkCollisions(enemies, aliens, explosions, ref score));
            explosions.RemoveAll(i => !i.Update(gameTime.ElapsedGameTime.Milliseconds, world.scrollSpeed));
            fuels.RemoveAll(i => i.checkCollisions(player.ship, gauge, ref score));
            fuels.ForEach(i => i.update(world.scrollSpeed));
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.SetRenderTarget(worldRenderTarget);
            spriteBatch.Begin();
            world.draw(spriteBatch);
            enemies.ForEach(i => i.draw(spriteBatch));
            aliens.ForEach(i => i.draw(spriteBatch));
            spriteBatch.End();

            GraphicsDevice.SetRenderTarget(renderTarget);
            spriteBatch.Begin();
            spriteBatch.Draw(worldRenderTarget, new Vector2(0, 0), worldRenderTarget.Bounds, Color.White, (float)0, new Vector2(0, 0), (float)1, SpriteEffects.None, (float)0);
            fuels.ForEach(i => i.draw(spriteBatch));
            missiles.ForEach(i => i.draw(spriteBatch));
            player.draw(spriteBatch);
            explosions.ForEach(i => i.Draw(spriteBatch, assets));
            gauge.draw(spriteBatch);
            spriteBatch.DrawString(assets.font, "score: " + (score + world.y / 100), new Vector2(5, 0), Color.Black);
            if (gameOver)
            {
                spriteBatch.DrawString(assets.font, assets.gameOverText, new Vector2(virtualWidth / 2 - assets.gameOverTextBounds.X / 2, virtualHeight / 2 - assets.gameOverTextBounds.Y / 2), Color.Black);
            }
            spriteBatch.End();

            GraphicsDevice.SetRenderTarget(null);
            Resolution.BeginDraw();
            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, Resolution.getTransformationMatrix());
            spriteBatch.Draw(renderTarget, new Vector2(0, 0), renderTarget.Bounds, Color.White, (float)0, new Vector2(0, 0), (float)1, SpriteEffects.None, (float)0);
            spriteBatch.End();
            
            base.Draw(gameTime);
        }
    }
}
