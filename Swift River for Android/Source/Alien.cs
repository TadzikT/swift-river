using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Swift_River_for_Android
{
    public class Alien
    {
        Assets assets;
        public Rectangle alienRect = new Rectangle();
        int speed = 3;

        public Alien(int x, int y, Assets assets)
        {
            this.assets = assets;
            alienRect.Width = assets.alien.Width;
            alienRect.Height = assets.alien.Height;
            alienRect.X = x - alienRect.Width / 2;
            alienRect.Y = y - alienRect.Height / 2;
        }

        public bool update(int scrollSpeed)
        {
            alienRect.Y += scrollSpeed + speed;
            if (alienRect.Y > 1000)
            {
                return false;
            }
            return true;
        }

        public void draw(SpriteBatch batch)
        {
            batch.Draw(assets.alien, new Vector2(alienRect.X, alienRect.Y), assets.alien.Bounds, Color.White, (float)0, new Vector2(0, 0), (float)1, SpriteEffects.None, (float)0);
        }

    }
}