﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace Swift_River_for_Android
{
    public class Player
    {
        public int screenWidth, screenHeight;
        public Rectangle ship;
        public int xSpeed = 3;
        public int direction; // 0 = left, 1 = straight, 2 = right 
        Color[] colorsFromWorldRenderTarget;
        Color[] colorsFromShipTexture;
        Rectangle shipRect;
        Assets assets;

        public Player(int screenWidth, int screenHeight, Assets assets)
        {
            this.screenWidth = screenWidth;
            this.screenHeight = screenHeight;
            ship = new Rectangle();
            ship.Width = assets.players[0].Width;
            ship.Height = assets.players[0].Height;
            ship.X = screenWidth / 2 - ship.Width / 2;
            ship.Y = screenHeight - ship.Height - 10;
            this.assets = assets;
            shipRect = new Rectangle(0, 0, ship.Width, ship.Height);
            colorsFromWorldRenderTarget = new Color[ship.Width * ship.Height];
            colorsFromShipTexture = new Color[shipRect.Width * shipRect.Height];

        }

        // returns false if ship collides with land
        public bool update(RenderTarget2D worldRenderTarget)
        {
            if (direction == 0)
            {
                ship.X -= xSpeed;
            }
            else if (direction == 2)
            {
                ship.X += xSpeed;
            }
            if (ship.X < 0)
            {
                ship.X = 0;
            }
            else if (ship.Right > screenWidth)
            {
                ship.X = screenWidth - ship.Width;
            }
            worldRenderTarget.GetData<Color>(0, ship, colorsFromWorldRenderTarget, 0, colorsFromWorldRenderTarget.Length);
            if (colorsFromWorldRenderTarget[0] == Utils.whiteColor) // this means worldRenderTarget is not rendered yet, the game just started, so skip checking collisions
            {
                return true;
            }
            assets.players[direction].GetData<Color>(0, shipRect, colorsFromShipTexture, 0, colorsFromShipTexture.Length);
            for (int i = 0; i < colorsFromShipTexture.Length; i++)
            {
                if (colorsFromShipTexture[i] != Utils.whiteColor)
                {
                    if (colorsFromWorldRenderTarget[i] != Utils.waterColor)
                    {
                        assets.sound(assets.playerCrashed).Play();
                        return false;
                    }
                }
            }
            return true;

        }

        public void draw(SpriteBatch batch)
        {
            batch.Draw(assets.players[direction], ship, Color.White);
        }
    }
}
