﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;

namespace Swift_River_for_Android
{
    public class Assets
    {
        Texture2D[] worldRegions = new Texture2D[10];
        public Texture2D[] players = new Texture2D[3];
        public Texture2D missile;
        public Texture2D enemy;
        public Texture2D explosion;
        public Texture2D gauge;
        public Texture2D pointer;
        public Texture2D fuel;
        public Texture2D alien;
        public SpriteFont font;
        public String gameOverText = "game over";
        public Vector2 gameOverTextBounds;

        public SoundEffect missileFired;
        public SoundEffect enemyHit;
        public SoundEffect fuelPicked;
        public SoundEffect airplaneFlying;
        public SoundEffect playerCrashed;
        public Song backgroundMusic;

        public Assets(ContentManager content)
        {
            for (int i = 0; i < worldRegions.Length; i++)
            {
                worldRegions[i] = content.Load<Texture2D>((i + 1).ToString()); 
            }
            players[0] = content.Load<Texture2D>("player_left");
            players[1] = content.Load<Texture2D>("player");
            players[2] = content.Load<Texture2D>("player_right");
            missile = content.Load<Texture2D>("missile");
            enemy = content.Load<Texture2D>("enemy");
            explosion = content.Load<Texture2D>("explosion");
            gauge = content.Load<Texture2D>("gauge");
            pointer = content.Load<Texture2D>("pointer");
            fuel = content.Load<Texture2D>("fuel");
            alien = content.Load<Texture2D>("alien");
            font = content.Load<SpriteFont>("reputation");
            gameOverTextBounds = font.MeasureString(gameOverText);

            missileFired = content.Load<SoundEffect>("MissileSound");
            enemyHit = content.Load<SoundEffect>("HitSound");
            fuelPicked = content.Load<SoundEffect>("FuelSound");
            airplaneFlying = content.Load<SoundEffect>("FlySound");
            playerCrashed = content.Load<SoundEffect>("CrashSound");
            backgroundMusic = content.Load<Song>("BackgroundMusic");
        }
        
        // n must be in range of 1 - 10 inclusive
        public Texture2D getWorldRegion(int n)
        {
            return worldRegions[n - 1];
        }

        public SoundEffectInstance sound(SoundEffect soundEffect)
        {
            SoundEffectInstance soundEffectInstance = soundEffect.CreateInstance();
            return soundEffectInstance;
        }

    }
}
