﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swift_River_for_Android
{
    public class Gauge
    {
        const int MAX_FUEL = 31000;
        Rectangle gaugeRect = new Rectangle();
        Assets assets;
        public int fuel = MAX_FUEL;
        float radian;

        public Gauge(int screenWidth, int screenHeight, Assets assets)
        {
            this.assets = assets;
            gaugeRect.Width = assets.gauge.Width;
            gaugeRect.Height = assets.gauge.Height;
            gaugeRect.X = screenWidth - gaugeRect.Width - 10;
            gaugeRect.Y = 10;
        }

        public bool update(int scrollSpeed)
        {
            fuel -= scrollSpeed;
            // zasieg rotacji pointera jest od -1.55 do 1.55
            radian = ((float)fuel / 10000) - 1.55f;
            if (fuel < 0)
            {
                return false;
            }
            return true;
        }

        public void addFuel(int count)
        {
            fuel += count;
            if (fuel > MAX_FUEL)
            {
                fuel = MAX_FUEL;
            }
        }

        public void draw(SpriteBatch batch)
        {
            batch.Draw(assets.gauge, gaugeRect, Color.White * 0.75f);
            batch.Draw(assets.pointer, new Vector2(gaugeRect.X + 60, gaugeRect.Y + 64), null, Color.White, radian, new Vector2(60, 64), 1f, SpriteEffects.None, 0f);
        }
    }
}
