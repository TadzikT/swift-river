﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swift_River
{
    public class Explosion
    {
        static private Random random = new Random();
        class Particle
        {
            public Rectangle rect;
            Vector2 vel;
            public float life, maxLife;

            public Particle(Vector2 pos)
            {
                life = (float)(random.NextDouble() * 300f + 600f);
                this.maxLife = life;
                vel = new Vector2((float)(random.NextDouble() * 10 - 5), (float)(random.NextDouble() * 10 - 5));
                rect = new Rectangle((int)pos.X, (int)pos.Y, 25, 25);
            }

            public void Update(float delta, int scrollSpeed)
            {
                if (life > 0)
                {
                    rect.X += (int)vel.X;
                    rect.Y += (int)vel.Y + scrollSpeed;
                    life -= delta;
                }
            }

        }

        static private int particleNum = 60;
        private List<Particle> particles = new List<Particle>();

        public Explosion(Vector2 pos)
        {
            for (int i = 0; i < particleNum; ++i)
            {
                particles.Add(new Particle(pos));
            }
        }

        public bool Update(float delta, int scrollSpeed)
        {
            foreach (Particle item in particles)
            {
                item.Update(delta, scrollSpeed);
                if (item.life < 0)
                {
                    return false;
                }
            }
            return true;
        }

        public void Draw(SpriteBatch batch, Assets assets)
        {
            foreach (Particle item in particles)
            {
                batch.Draw(assets.explosion, item.rect, Color.White * (item.life / item.maxLife));
            }

        }

    }
}
