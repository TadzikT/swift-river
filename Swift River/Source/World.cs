﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swift_River
{
    /// kawalki mapy ktore pasuja ze soba (dolna ich czesc z gorna innego kawalku) i w jakich pikselach sa granice ląd\rzeka (albo rzeka\ląd) u gory kawalku mapy>
    /// 1: łączy się z 4, 6.        87, 347.
    /// 2: łączy się z 1, 5, 8.     15, 203, 273, 445.
    /// 3: łączy się z 1, 5, 8.     260, 445.
    /// 4: łączy się z 3, 7.        73, 348.
    /// 5: łączy się z 3, 7.        87, 347.
    /// 6: łączy się z 3, 7.        73, 348.
    /// 7: łączy się z 2, 9, 10.    260, 445.
    /// 8: łączy się z 2, 9, 10.    87, 347.
    /// 9: łączy się z 4, 6         15, 203, 273, 445.
    /// 10: łączy się z 1, 5, 8.    15, 203, 273, 445.
    /// 
    public class World
    {
        Assets assets;
        int screenWidth, screenHeight;
        Random random = new Random();
        public int scrollSpeed = 5;
        public int y = 0;
        public int textureFlipsCounter = 0;
        bool whichRegionOnTop = false; // true is worldRegion, false is otherWorldRegion
        public int textureHeight;
        int lastYWhenFlipped = 0;

        int worldRegionNumber;
        Texture2D worldRegion;

        int otherWorldRegionNumber;
        Texture2D otherWorldRegion;

        public World(int screenWidth, int screenHeight, Assets assets)
        {
            this.screenWidth = screenWidth;
            this.screenHeight = screenHeight;
            this.assets = assets;
            worldRegionNumber = 1;
            worldRegion = assets.getWorldRegion(worldRegionNumber);
            otherWorldRegionNumber = getNextWorldRegionNumber(worldRegionNumber);
            otherWorldRegion = assets.getWorldRegion(otherWorldRegionNumber);
            textureHeight = worldRegion.Height;
        }

        public void update(List<Enemy> enemies, List<Fuel> fuels)
        {
            y += scrollSpeed;
            if ((y - lastYWhenFlipped) >= 1000)
            {
                if (whichRegionOnTop)
                {
                    otherWorldRegionNumber = getNextWorldRegionNumber(worldRegionNumber);
                    otherWorldRegion = assets.getWorldRegion(otherWorldRegionNumber);
                }
                else if (!whichRegionOnTop)
                {
                    worldRegionNumber = getNextWorldRegionNumber(otherWorldRegionNumber);
                    worldRegion = assets.getWorldRegion(worldRegionNumber);
                }
                whichRegionOnTop = !whichRegionOnTop;
                textureFlipsCounter++;
                lastYWhenFlipped = y;
                for (int i = 0; i < random.Next(5, 10); i++)
                {
                    enemies.Add(new Enemy(random.Next(screenWidth), -random.Next(1000, 2000), random.Next(2) == 0, assets));
                }
                for (int i = 0; i < random.Next(2, 6); i++)
                {
                    fuels.Add(new Fuel(random.Next(screenWidth), -random.Next(1000, 2000), assets));
                }
            }
        }

        public void draw(SpriteBatch batch)
        {
            if (whichRegionOnTop)
            {
                batch.Draw(otherWorldRegion, new Vector2(0, y - textureHeight - textureFlipsCounter * textureHeight + screenHeight), Color.White);
                batch.Draw(worldRegion, new Vector2(0, y - textureHeight * 2 - textureFlipsCounter * textureHeight + screenHeight), Color.White);
            }
            else if (!whichRegionOnTop)
            {
                batch.Draw(worldRegion, new Vector2(0, y - textureHeight - textureFlipsCounter * textureHeight + screenHeight), Color.White);
                batch.Draw(otherWorldRegion, new Vector2(0, y - textureHeight * 2 - textureFlipsCounter * textureHeight + screenHeight), Color.White);
            }
        }

        int getNextWorldRegionNumber(int currentWorldRegion)
        {
            if (new[] { 1, 5, 8 }.Contains(currentWorldRegion))
            {
               return randomElement(new[] { 2, 3, 10 });
            }
            else if (new[] { 2, 9, 10 }.Contains(currentWorldRegion))
            {
               return randomElement(new[] { 7, 8 });
            }
            else if (new[] { 3, 7 }.Contains(currentWorldRegion))
            {
                return randomElement(new[] { 4, 5, 6 });
            }
            else if (new[] { 4, 6 }.Contains(currentWorldRegion))
            {
                return randomElement(new[] { 1, 9 });
            }
            else
            {
                return 0;
            }
        }

        int randomElement(int[] array)
        {
            return array[random.Next(array.Length)];
        }
    }
}
