﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swift_River
{
    public class Enemy
    {
        Assets assets;
        public Rectangle enemyRect = new Rectangle();
        int speed = 3;
        bool direction; // true = going right. false = going left.

        public Enemy(int x, int y, bool direction, Assets assets)
        {
            this.assets = assets;
            this.direction = direction;
            enemyRect.Width = assets.enemy.Width;
            enemyRect.Height = assets.enemy.Height;
            enemyRect.X = x - enemyRect.Width / 2;
            enemyRect.Y = y - enemyRect.Height / 2;
        }

        public bool update(int scrollSpeed, int screenWidth, int textureFlipsCounter, int worldTextureHeight)
        {
            enemyRect.Y += scrollSpeed;
            if (direction)
            {
                enemyRect.X += speed;
            }
            else if (!direction)
            {
                enemyRect.X -= speed;
            }
            if (enemyRect.X < 0)
            {
                enemyRect.X = 0;
                direction = !direction;
            }
            else if (enemyRect.Right > screenWidth)
            {
                enemyRect.X = screenWidth - enemyRect.Width;
                direction = !direction;
            }
            if (enemyRect.Y > 1000)
            {
                return false;
            }
            return true;
        }

        public void draw(SpriteBatch batch)
        {
            batch.Draw(assets.enemy, new Vector2(enemyRect.X, enemyRect.Y), assets.enemy.Bounds, Color.White, (float)0, new Vector2(0, 0), (float)1, direction ? SpriteEffects.FlipHorizontally : SpriteEffects.None, (float)0);
        }

    }
}
