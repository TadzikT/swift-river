﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swift_River
{
    public class Missile
    {
        static TimeSpan lastTimeWhenLaunched;
        static TimeSpan cooldown = new TimeSpan(0, 0, 0, 0, 500);
        Rectangle missileRect = new Rectangle();
        Assets assets;
        int speed = 15;


        public Missile(int x, int y, Assets assets)
        {
            this.assets = assets;
            missileRect.Width = assets.missile.Width;
            missileRect.Height = assets.missile.Height;
            missileRect.X = x - missileRect.Width / 2;
            missileRect.Y = y - missileRect.Height / 2;
        }

        public bool update(int scrollSpeed)
        {
            missileRect.Y -= speed - scrollSpeed;
            if (missileRect.Y < -200)
            {
                return false;
            }
            return true;
        }

        public void draw(SpriteBatch batch)
        {
            batch.Draw(assets.missile, missileRect, Color.White);
        }

        static public bool readyToLaunch(TimeSpan timeSpan)
        {

            if (timeSpan.CompareTo(lastTimeWhenLaunched + cooldown) > 0)
            {
                lastTimeWhenLaunched = timeSpan;
                return true;
            }
            return false;
        }

        public bool checkCollisions(List<Enemy> enemies, List<Explosion> explosions, ref int score)
        {
            foreach (Enemy enemy in enemies)
            {
                if (missileRect.Intersects(enemy.enemyRect))
                {
                    explosions.Add(new Explosion(new Vector2(enemy.enemyRect.X + enemy.enemyRect.Width / 2, enemy.enemyRect.Y + enemy.enemyRect.Height / 2)));
                    enemies.Remove(enemy);
                    assets.sound(assets.enemyHit).Play();
                    score += 100;
                    return true;
                }
            }
            return false;
        }
    }
}
