﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swift_River
{
    public class Fuel
    {
        Rectangle fuelRect = new Rectangle();
        Assets assets;

        public Fuel(int x, int y, Assets assets)
        {
            this.assets = assets;
            fuelRect.Width = assets.fuel.Width;
            fuelRect.Height = assets.fuel.Height;
            fuelRect.X = x - fuelRect.Width / 2;
            fuelRect.Y = y - fuelRect.Height / 2;
        }

        public bool update(int scrollSpeed)
        {
            fuelRect.Y += scrollSpeed;
            if (fuelRect.Y < 1000)
            {
                return false;
            }
            return true;
        }

        //true if collided, false if not
        public bool checkCollisions(Rectangle ship, Gauge gauge, ref int score)
        {

            if (fuelRect.Intersects(ship))
            {
                gauge.addFuel(500);
                assets.sound(assets.fuelPicked).Play();
                score += 200;
                return true;
            }
            return false;
        }

        public void draw(SpriteBatch batch)
        {
            batch.Draw(assets.fuel, fuelRect, Color.White * 0.75f);
        }
    }
}
