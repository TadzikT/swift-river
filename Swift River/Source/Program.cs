﻿using System;

namespace Swift_River
{
#if WINDOWS || LINUX
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new SwiftRiver())
                game.Run();
        }
    }
#endif
}
