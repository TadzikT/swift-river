﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace Swift_River
{

    public class SwiftRiver : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        RenderTarget2D renderTarget;
        RenderTarget2D worldRenderTarget;
        Assets assets;
        World world;
        Player player;
        List<Missile> missiles;
        List<Enemy> enemies;
        List<Explosion> explosions;
        List<Fuel> fuels;
        Gauge gauge;
        bool gameOver;
        public int score;

        public SwiftRiver()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = 450;
            graphics.PreferredBackBufferHeight = 800;
            graphics.ApplyChanges();
            renderTarget = new RenderTarget2D(graphics.GraphicsDevice, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, false, graphics.PreferredBackBufferFormat, DepthFormat.Depth24);
            worldRenderTarget = new RenderTarget2D(graphics.GraphicsDevice, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, false, graphics.PreferredBackBufferFormat, DepthFormat.Depth24);
        }

        protected override void Initialize()
        {
            
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            assets = new Assets(Content);
            initGame();
        }

        void initGame()
        {
            missiles = new List<Missile>();
            enemies = new List<Enemy>();
            explosions = new List<Explosion>();
            fuels = new List<Fuel>();
            world = new World(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, assets);
            player = new Player(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, assets);
            gauge = new Gauge(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, assets);
            score = 0;
            gameOver = false;
            SoundEffect.MasterVolume = 0.3f;
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }
            if (gameOver)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Space))
                {
                    initGame();
                }
                else
                {
                    return;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                player.direction = 0;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                player.direction = 2;
            }
            else
            {
                player.direction = 1;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                world.scrollSpeed = 8;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                world.scrollSpeed = 2;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.T))
            {
                world.scrollSpeed = 0;
            }
            else
            {
                world.scrollSpeed = 5;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.LeftControl))
            {
                if (Missile.readyToLaunch(gameTime.TotalGameTime))
                {
                    assets.sound(assets.missileFired).Play();
                    missiles.Add(new Missile(player.ship.X + 33, player.ship.Y - 10, assets));
                    missiles.Add(new Missile(player.ship.X + 75, player.ship.Y - 10, assets));
                }
            }

            world.update(enemies, fuels);
            missiles.RemoveAll(i => !i.update(world.scrollSpeed));
            enemies.RemoveAll(i => !i.update(world.scrollSpeed, graphics.PreferredBackBufferWidth, world.textureFlipsCounter, world.textureHeight));
            if (!player.update(worldRenderTarget) || !gauge.update(world.scrollSpeed))
            {
                gameOver = true;
            }
            else
            {
                gameOver = false;
            }
            missiles.RemoveAll(i => i.checkCollisions(enemies, explosions, ref score));
            explosions.RemoveAll(i => !i.Update(gameTime.ElapsedGameTime.Milliseconds, world.scrollSpeed));
            fuels.RemoveAll(i => i.checkCollisions(player.ship, gauge, ref score));
            fuels.ForEach(i => i.update(world.scrollSpeed));
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            GraphicsDevice.SetRenderTarget(worldRenderTarget);
            spriteBatch.Begin();
            world.draw(spriteBatch);
            enemies.ForEach(i => i.draw(spriteBatch));
            spriteBatch.End();

            GraphicsDevice.SetRenderTarget(renderTarget);
            spriteBatch.Begin();
            spriteBatch.Draw(worldRenderTarget, new Vector2(0, 0), worldRenderTarget.Bounds, Color.White, (float)0, new Vector2(0, 0), (float)1, SpriteEffects.None, (float)0);
            fuels.ForEach(i => i.draw(spriteBatch));
            missiles.ForEach(i => i.draw(spriteBatch));
            player.draw(spriteBatch);
            explosions.ForEach(i => i.Draw(spriteBatch, assets));
            gauge.draw(spriteBatch);
            spriteBatch.DrawString(assets.font, "score: " + (score + world.y / 100), new Vector2(5, 0), Color.Black);
            if (gameOver)
            {
                spriteBatch.DrawString(assets.font, assets.gameOverText, new Vector2(graphics.PreferredBackBufferWidth / 2 - assets.gameOverTextBounds.X / 2, graphics.PreferredBackBufferHeight / 2 - assets.gameOverTextBounds.Y / 2), Color.Black);
            }
            spriteBatch.End();

            GraphicsDevice.SetRenderTarget(null);
            spriteBatch.Begin();
            spriteBatch.Draw(renderTarget, new Vector2(0, 0), renderTarget.Bounds, Color.White, (float)0, new Vector2(0, 0), (float)1, SpriteEffects.None, (float)0);
            spriteBatch.End();
            
            base.Draw(gameTime);
        }
    }
}
